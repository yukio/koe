FROM --platform=linux/amd64 node:10-buster-slim as frontend
LABEL Author="Yukio Fukuzawa"

WORKDIR /code
COPY package.json package-lock.json /code/ 
RUN apt-get update && apt-get install -y libvips-dev git ca-certificates --no-install-recommends
RUN npm install
COPY .babelrc webpack.base.config.js webpack.prod.config.js build.js server.js .bootstraprc /code/
COPY assets /code/assets
# RUN rm -rf /code/assets/bundles
RUN rm -rf /code/assets/bundles && npm run-script build-prod

FROM python:3.9.19 as backend
LABEL Author="Yukio Fukuzawa"

ENV PYTHONUNBUFFERED 1

ARG uid=1000
ARG http_proxy=''
ARG https_proxy=''
ENV http_proxy=''
ENV https_proxy=''
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# Install extra packages.
RUN apt update && \
    apt-get install -y \
    --no-install-recommends \
    build-essential vim mariadb-client ffmpeg libxml2-dev libxmlsec1-dev libpcre3-dev gfortran \
    && rm -rf /var/lib/apt/lists/*

COPY pyproject.toml poetry.lock poetry-install.py requirements-production.txt /

# ## Install any needed packages specified in requirements.txt
# RUN python3 -m pip install --upgrade pip
# RUN pip3 uninstall -y numpy scipy

# Install Poetry
# COPY poetry-install.py /
RUN python3 poetry-install.py
ENV PATH="/root/.local/bin:${PATH}"

# COPY pyproject.toml poetry.lock /

# Install dependencies using Poetry
RUN poetry config virtualenvs.create false && poetry install --without dev

# COPY requirements-production.txt /
RUN pip3 install -r requirements-production.txt --no-cache-dir

# Make port 8000 available to the world outside this container
EXPOSE 8000

WORKDIR /code

COPY . .

RUN rm -rf assets/bundles static
COPY --from=frontend /code/assets/bundles /code/assets/bundles
COPY --from=frontend /code/jquery-webpack-stats.json /code/webpack-stats.json  /code/
# COPY --from=frontend /code/webpack-stats.json /code/webpack-stats.json 
COPY --from=frontend /code/node_modules /code/node_modules 

ENV DEBUG=False
ARG DEBUG=False
RUN python manage.py collectstatic --noinput
RUN rm -rf /code/node_modules

ENTRYPOINT ["/bin/bash"]
CMD ["run_koe_app.sh"]
